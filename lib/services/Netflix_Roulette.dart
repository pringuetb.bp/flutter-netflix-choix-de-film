import 'dart:convert';

import 'package:Netflix_Roulette/models/Content.dart';
import 'package:http/http.dart' as http;

class Netflix_Roulette {
  Future getRandom() async {
    var response = await http
        .get("https://api.reelgood.com/v3.0/content/random?sources=netflix");

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);

      Content content = new Content(data['id'], data['slug'], data['title'], data['released_on'], data['overview'], data['content_type'], data['has_poster'], data['season_count']);

      return content;
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }
}

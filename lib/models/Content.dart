class Content {
  String _id, _slug, _title, _released_on, _overview, _content_type, _poster;
  bool _has_poster;
  int _season_count;

  Content(id, slug, title, released_on, overview,
      content_type, has_poster, season_count) {
    this._id = id;
    this._slug = slug;
    this._title = title;
    this._released_on = released_on;
    this._overview = overview;
    this._content_type = content_type;
    this._has_poster = has_poster;
    this._season_count = season_count;
    if (this._has_poster) {
     if (this._content_type == 'm') {
       this._poster = 'https://img.reelgood.com/content/movie/${this._id}/poster-780.jpg';
     } else {
       this._poster = 'https://img.reelgood.com/content/show/${this._id}/poster-780.jpg';
     }
    } else {
      this._poster = null;
    }
  }

  int get season_count => _season_count;

  set season_count(int value) {
    _season_count = value;
  }

  bool get has_poster => _has_poster;

  set has_poster(bool value) {
    _has_poster = value;
  }

  get content_type => _content_type;

  set content_type(value) {
    _content_type = value;
  }

  get overview => _overview;

  set overview(value) {
    _overview = value;
  }

  get released_on => _released_on;

  set released_on(value) {
    _released_on = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  get slug => _slug;

  set slug(value) {
    _slug = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  get poster => _poster;

  set poster(value) {
    _poster = value;
  }
}

import 'package:Netflix_Roulette/services/Netflix_Roulette.dart';
import 'package:flutter/material.dart';

class MainContent extends StatelessWidget {
  Netflix_Roulette _Netflix_Roulette = new Netflix_Roulette();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: FutureBuilder(
        future: this._Netflix_Roulette.getRandom(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (snapshot.data.poster != null)
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                    child: Image.network(snapshot.data.poster),
                  ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    snapshot.data.title,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Text(
                    snapshot.data.overview,
                  ),
                ),
              ],
            );
          }
          else {
            return Center(
              child: CircularProgressIndicator()
            );
          }
        },
      ),
    );
  }
}
